#!/bin/bash
# -*- Mode: sh; indent-tabs-mode: nil; tab-width: 2 -*-

# Ensure the shell always gets unthrottled touch events, so that applications
# who want full speed low-latency input can get it (LP: #1497105) and so that
# apps can use QML touch compression safely (the QML touch compression
# algorithm does not support nesting well - LP: #1486341, LP: #1556763 - so
# must be fed by the raw input event stream from Unity8).
export QML_NO_TOUCH_COMPRESSION=1

export MIR_SERVER_PROMPT_FILE=1

# Hard code socket path because our snappy apparmor profile
# only lets us put the socket in one place.  And consumers expect it there.
# (XDG_RUNTIME_DIR isn't typical under snappy)
export MIR_SERVER_FILE=/run/user/$(id -u)/mir_socket

rm -f "$MIR_SERVER_FILE"
rm -f "${MIR_SERVER_FILE}_trusted"

if [ "$XDG_SESSION_DESKTOP" = "ubuntu-touch" ]; then
  # On Ubuntu Touch, we currently use a trick where we auto-login the
  # user without a LightDM greeter at all.  In this case, we want to
  # start with the lockscreen visible.  Once we switch to using a
  # proper greeter for the first login, we can remove this code path.
  MODE=full-greeter
else
  MODE=full-shell
fi

# Add options for settings
if [ -f ~/.unity8/configs.conf ]; then
  source ~/.unity8/configs.conf
fi

# Scale settings
isfloat='^[0-9]+([.][0-9]+)?$'
isint='^[0-9]+$'
if [[ $SCALE =~ $isfloat ]]; then
  if ! [ $SCALE == "0" ]; then
    export GRID_UNIT_PX=$(LC_ALL=C printf %.0f $((8 * ${SCALE})))
    export QT_WAYLAND_FORCE_DPI=$(LC_ALL=C printf %.0f $((96 * ${SCALE})))
  fi
fi

if [[ $GRID_UNIT_PX =~ $isint ]]; then
  dbus-update-activation-environment --systemd GRID_UNIT_PX=${GRID_UNIT_PX}
fi

if [[ $QT_WAYLAND_FORCE_DPI =~ $isint ]]; then
  dbus-update-activation-environment --systemd QT_WAYLAND_FORCE_DPI=${QT_WAYLAND_FORCE_DPI}
fi

# Tell unity-mir to raise SIGSTOP after we start, which we use to tell
# systemd when we are done starting up.
export UNITY_MIR_EMITS_SIGSTOP=1

# Set some envs
dbus-update-activation-environment --systemd QT_QPA_PLATFORM=wayland
dbus-update-activation-environment --systemd GDK_BACKEND=wayland
dbus-update-activation-environment --systemd SDL_VIDEODRIVER=wayland
dbus-update-activation-environment --systemd UNITY8_SHELL_QT_QPA_PLATFORM=mirserver
dbus-update-activation-environment --systemd QT_WAYLAND_DISABLE_WINDOWDECORATION=1
dbus-update-activation-environment --systemd QT_ACCESSIBILITY=1
dbus-update-activation-environment --systemd QT_AUTO_SCREEN_SCALE_FACTOR=0
dbus-update-activation-environment --systemd GTK_CSD=0

# We need to be sure we dont use mir-on-mir
unset MIR_SERVER_HOST_SOCKET
unset DISPLAY

QT_QPA_PLATFORM=${UNITY8_SHELL_QT_QPA_PLATFORM} ${BINARY:-unity8} --mode=$MODE "$@" &
child=$!

# Wait for SIGSTOP in child
while [ $(ps --no-headers -o state $child) != "T" ]; do
  sleep 0.1
done

kill -CONT $child
exit 0
